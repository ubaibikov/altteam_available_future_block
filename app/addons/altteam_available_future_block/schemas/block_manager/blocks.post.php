<?php
/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/

$schema['products']['content']['items']['fillings']['future_available'] = array (
    'params' => array (
        'future_available' => true,
        'sort_order' => 'desc',
        'request' => array (
            'main_product_id' => '%PRODUCT_ID%'
        )
    ),
);


return $schema;
