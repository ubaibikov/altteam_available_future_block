<?php
/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/


if (!defined('BOOTSTRAP')) { die('Access denied'); }



function fn_altteam_available_future_block_get_products(&$params, &$fields, &$sortings, &$condition, &$join, &$sorting, &$group_by, &$lang_code, &$having)
{
    if(!empty($params['future_available']))
    { 
        $condition .= db_quote(" AND products.avail_since > ?i", time());
    }
}